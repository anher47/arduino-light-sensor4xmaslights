int sensorPin = A0; 
int ledPin = 2;
int sensorValue = 0;
const int MAXLIGHTVALUE = 500;

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600); 
}

void loop() {
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  if(sensorValue > MAXLIGHTVALUE) {
      digitalWrite(ledPin, HIGH);
    } else {
      digitalWrite(ledPin, LOW);
    }
  
  delay(100);
}